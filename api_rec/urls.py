from django.contrib import admin
from django.urls import include, path

from rest_framework import routers
from kapital_api.views import UserRegistration,LoginView,password_reset,password_reset_confirm,UpdateCandidatView,UpdateRecruteurView
from django.conf import settings
from django.conf.urls.static import static
from kapital_api.urls import router as PosteRouter
from rest_framework_simplejwt.views import TokenRefreshView

router = routers.DefaultRouter()
router.registry.extend(PosteRouter.registry)
urlpatterns = [
    path('admin/',admin.site.urls),
    path('api/candidat/update/',UpdateCandidatView.as_view()),
    path('api/recruteur/update/',UpdateRecruteurView.as_view()),
    path('api/password-reset/', password_reset, name='password_reset'),
    path('api/password-reset-confirm/<crypt_user_id>/<token>/',password_reset_confirm),
    path('api-auth/',include('rest_framework.urls')),
    path('api/register/', UserRegistration.as_view()),
    path('api/token/',LoginView.as_view(), name ='token_obtain_pair'),
    path('api/token/refresh/',TokenRefreshView.as_view(),name = 'token_refresh'),
    path('api/',include(router.urls)),
]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
