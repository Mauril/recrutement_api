from datetime import datetime, timedelta
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

class CustomUserManager(BaseUserManager):
    def _create_user(self,email, password,**extra_fields):
        if not email:
            raise ValueError("Email must be provided")
        if not password:
            raise ValueError('Password is not provided')
        user = self.model(
            email = self.normalize_email(email),
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff',False)
        extra_fields.setdefault('is_active',True)
        extra_fields.setdefault('is_superuser',False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff',True)
        extra_fields.setdefault('is_active',True)
        extra_fields.setdefault('is_superuser',True)
        return self._create_user(email, password, **extra_fields)

class CustomUser(AbstractBaseUser,PermissionsMixin):
    class Role(models.TextChoices):
        candidat = 'candidat'
        recruteur = 'recruteur'
    nom = models.CharField(max_length=100,null=True,blank=True)
    prenoms = models.CharField(max_length=100,null=True,blank=True)
    telephone = models.IntegerField(null=True,blank=True)
    adresse = models.CharField(max_length=255,default='',blank=True)
    email = models.EmailField(unique=True,max_length=255)
    role = models.CharField(max_length=15,choices=Role.choices,null=True,blank=True)
    pays=models.CharField(max_length=50,default='',blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    objects = CustomUserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS=[]

    def __str__(self):
        return self.email


class Poste(models.Model):
    nom = models.CharField(max_length=100)
    def __str__(self):
        return self.nom



class CentreInteret(models.Model):
    nom = models.CharField(max_length=50)
    def __str__(self):
        return self.nom

class Competence(models.Model):
    nom = models.CharField(50)
    def __str__(self):
        return self.nom

class NiveauEtude(models.Model):
    niveau_etude = models.CharField(max_length=25)
    def __str__(self):
        return self.niveau_etude


class Recruteur(CustomUser):
    def upload_logo(instance,filename):
        return '/'.join(['logo',str(instance.nom),filename])
    nom_entreprise = models.CharField(max_length=100,default='',blank=True)
    secteur_activite = models.CharField(max_length=100,default='',blank=True)
    logo = models.ImageField(upload_to=upload_logo,null=True,blank=True)
    nombre_enmploye =  models.IntegerField(null=True,blank=True)
    abonne = models.BooleanField(default=False)
    date_abonnement = models.DateTimeField(null=True,blank=True)
    description = models.TextField(default='',blank=True)
    is_particulier = models.BooleanField(default=True)
    numero_cnss = models.CharField(max_length=50,default='',blank=True)
    numero_ifu = models.CharField(max_length=200,default='',blank=True)
    registre_commerce = models.CharField(max_length=100,default='',blank=True)
    
    def __str__(self):
        return self.email

class Candidat(CustomUser):
    class Sexe(models.TextChoices):
        masculin='M'
        feminin = 'F'
    def upload_cv(instance,filename):
        return '/'.join(['cv',str(instance.nom),filename])
    def upload_lettre(instance,filename):
        return '/'.join(['lettres',str(instance.nom),filename])
    
    cv = models.FileField(upload_to=upload_cv, null=True,blank=True)
    lettre_motivation = models.FileField(upload_to=upload_lettre,null=True,blank=True)
    date_naissance = models.DateField(null=True,blank=True)
    sexe = models.CharField(max_length=2,choices=Sexe.choices,null=True,blank=True)
    niveau_etude = models.ForeignKey(NiveauEtude,on_delete=models.RESTRICT,related_name='candidat',blank=True,null=True)
    centre_interets = models.ManyToManyField(CentreInteret,blank=True)
    competences = models.ManyToManyField(Competence,blank=True)
    def __str__(self):
        return self.email

class EntretienVideoDifferee(models.Model):
    class Statut(models.TextChoices):
        en_cours='En cours'
        termine = 'Terminé'
    statut = models.CharField(max_length=20,choices=Statut.choices)
    statut = models.CharField(max_length=20)
    titre = models.CharField(max_length=50)
    description = models.TextField()
    date_expiration = models.DateTimeField()
    duree_minute = models.IntegerField()
    recruteur = models.ForeignKey(Recruteur,on_delete=models.RESTRICT,related_name='entretien_differee')
    def __str__(self):
        return self.titre


#a completer
class EntretienDirect(models.Model):
    class Statut(models.TextChoices):
        en_cours='En cours'
        termine = 'Terminé'
    statut = models.CharField(max_length=20,choices=Statut.choices)
    titre = models.CharField(max_length=50)
    description = models.TextField()
    date = models.DateTimeField()
    lien = models.TextField()
    recruteur = models.ForeignKey(Recruteur,on_delete=models.RESTRICT,related_name='entretien_direct')
    def __str__(self):
        return self.titre



class OffreEmploi(models.Model):
    class Statut(models.TextChoices):
        en_cours= 'En cours'
        ferme = 'Fermé'
    nom_entreprise = models.CharField(max_length=100)
    salaire = models.IntegerField()
    commentaire = models.TextField()
    date_publication = models.DateTimeField()
    localisation = models.CharField(max_length=100)
    type_contrat = models.CharField(max_length=20)
    statut = models.CharField(max_length=20,choices=Statut.choices)
    image = models.ImageField(null=True, upload_to='images/')
    date_limite = models.DateTimeField()
    langue_requise = models.CharField()
    age_requis = models.IntegerField()
    recruteur = models.ForeignKey(Recruteur,on_delete=models.RESTRICT,related_name='offre_emploi')
    poste = models.ForeignKey(Poste,on_delete=models.RESTRICT,related_name='offre_emploi')
    niveau_etude = models.ForeignKey(NiveauEtude,on_delete=models.RESTRICT,related_name='offre_emploi')
    centre_interet = models.ForeignKey(CentreInteret,on_delete=models.RESTRICT,related_name='offre_emploi')
    competences = models.ManyToManyField(Competence)
    candidatures = models.ManyToManyField(Candidat,through='Candidature')
    entretien_directs = models.ManyToManyField(EntretienDirect)
    entretien_video_differee = models.ManyToManyField(EntretienVideoDifferee)
    def __str__(self):
        return f" offre pour poste {self.poste}"
    


class ExperienceProfessionnelle(models.Model):
    nom_entreprise = models.CharField(max_length=200)
    date_debut = models.DateField()
    date_fin = models.DateField(null=True)
    lieu = models.CharField(max_length=100)
    description = models.TextField()
    candidat = models.ForeignKey(Candidat,null=True,on_delete=models.RESTRICT,related_name='experience_professionnelle')
    poste = models.ForeignKey(Poste,on_delete=models.RESTRICT,related_name='experience_professionnelle')

class FicheDePoste(models.Model):
    situation_de_poste = models.TextField()
    missions = models.TextField()
    activites = models.TextField()
    exigences = models.TextField()
    champ_relations = models.TextField()
    difficultes = models.TextField()
    evolution = models.TextField()
    offre_emploi = models.OneToOneField(OffreEmploi,primary_key=True,on_delete=models.RESTRICT,related_name='fiche_de_poste')

class Candidature(models.Model):
    class Statut(models.TextChoices):
        en_cours='En cours'
        embauche = 'Embauché'
        rejete = 'Rejeté'
    statut = models.CharField(max_length=20,choices=Statut.choices)
    date_postulation = models.DateTimeField()
    note = models.IntegerField()
    contestation = models.TextField()
    synthese = models.TextField()
    offre_emploi = models.ForeignKey(OffreEmploi,on_delete=models.RESTRICT)
    candidat = models.ForeignKey(Candidat,on_delete=models.RESTRICT)
    

class Video(models.Model):
    video = models.FileField()
    date_envoi = models.DateTimeField()
    entretien = models.ForeignKey(EntretienVideoDifferee,on_delete=models.RESTRICT,related_name='videos')
    candidature = models.ForeignKey(Candidature,on_delete=models.RESTRICT,related_name='videos')

class Evaluation(models.Model):
    nom = models.CharField(max_length=50)
    def __str__(self):
        return self.nom

class Question(models.Model):
    question = models.TextField()
    evaluation = models.ForeignKey(Evaluation,on_delete=models.RESTRICT,related_name='questions')
    def __str__(self):
        return self.question

class Reponse(models.Model):
    reponse = models.CharField(max_length=50)
    valid = models.BooleanField(default=False)
    question = models.ForeignKey(Question,on_delete=models.RESTRICT,related_name='reponses')
    
    def __str__(self):
        return self.reponse
    