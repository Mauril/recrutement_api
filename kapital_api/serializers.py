from rest_framework.serializers import ModelSerializer
from .models import CentreInteret, Competence, ExperienceProfessionnelle, NiveauEtude, Poste,Evaluation,Question, Recruteur,Reponse,CustomUser,Candidat
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class PosteSerializer(ModelSerializer):
    class Meta:
        model = Poste
        fields = '__all__'

class ExperienceProfessionnelleSerializer(serializers.ModelSerializer):
    poste=PosteSerializer()
    class Meta:
        model=ExperienceProfessionnelle
        fields='__all__'
        read_only_fields=['candidat']

    def create(self, validated_data):
        selected_poste=validated_data.pop('poste')
        poste=Poste.objects.get(nom=selected_poste.get('nom'))
        candidat=Candidat.objects.get(pk=self.context['request'].user)
        new_exp=ExperienceProfessionnelle.objects.create(
            nom_entreprise=validated_data['nom_entreprise'],
            date_debut=validated_data['date_debut'],
            date_fin=validated_data['date_fin'],
            lieu=validated_data['lieu'],
            description=validated_data['description'],
            poste=poste,
            candidat=candidat
            )
        return new_exp
    def update(self, instance, validated_data):
        selected_poste=validated_data.pop('poste')
        try:
            poste=Poste.objects.get(nom=selected_poste.get('nom'))
            instance.poste=poste
        except:
            pass
        for key,val in validated_data.items():
            setattr(instance,key,val)
        instance.save()
        return instance

class CompetenceSerializer(serializers.ModelSerializer):
    class Meta:
        model=Competence
        fields='__all__'
    def create(self, validated_data):
        return super().create(validated_data)
class CentreInteretSerializer(serializers.ModelSerializer):
    class Meta:
        model=CentreInteret
        fields='__all__'

class NiveauEtudeSerializer(serializers.ModelSerializer):
    class Meta:
        model= NiveauEtude
        fields='__all__'


class CandidatSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    class Meta:
        model = Candidat
        fields = ['email','password','role',]
    def create(self, validated_data):
        password = validated_data.pop('password')
        user = self.Meta.model(**validated_data)
        if password is not None : 
            user.set_password(password)
        user.save()
        return user

class RecruteurSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    class Meta:
        model = Recruteur
        fields = ['email','password','role']
    def create(self, validated_data):
        password = validated_data.pop('password')
        user = self.Meta.model(**validated_data)
        if password is not None : 
            user.set_password(password)
        user.save()
        return user
   
class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length = 255)

class PasswordResetConfirmSerializer(serializers.Serializer):
    password = serializers.CharField()

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        return token


class UpdateCandidatSerializer(ModelSerializer):
    centre_interets=CentreInteretSerializer(read_only=True,many=True)
    competences=CompetenceSerializer(required=False,many=True)
    experience_professionnelle=ExperienceProfessionnelleSerializer(read_only=True,many=True)
    class Meta:
        model = Candidat
        read_only_fields = ['email']
        exclude = ['password','is_active','is_staff','is_superuser','role','groups','user_permissions','last_login']
    

class UpdateRecruteurSerializer(ModelSerializer):
    is_particulier =serializers.BooleanField(source='recruteur.is_particulier',read_only=True)
    class Meta:
        model = Recruteur
        read_only_fields = ['email']
        exclude = ['password','is_active','is_staff','is_superuser','role','id','abonne','groups','user_permissions','last_login']


# class UserSerializer(ModelSerializer):
#     class Meta:
#         model = CustomUser
#         fields = '__all__'




class CentreInteretSerializer(ModelSerializer):
    class Meta:
        model=CentreInteret
        fields='__all__'

class ReponseSerializer(ModelSerializer):
    class Meta:
        model = Reponse
        fields = '__all__'

class QuestionSerializer(ModelSerializer):
    reponses = ReponseSerializer(many = True)
    class Meta:
        model = Question
        fields = ['id','question','reponses']


class EvaluationSerializer(ModelSerializer):
    questions = QuestionSerializer(many = True)
    class Meta:
        model = Evaluation
        fields = ['id','nom','questions']
