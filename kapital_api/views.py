from django.shortcuts import render,get_object_or_404
from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet
from rest_framework.generics import UpdateAPIView
from rest_framework.parsers import MultiPartParser,FormParser
import json

from .serializers import (CentreInteretSerializer, CompetenceSerializer, ExperienceProfessionnelleSerializer, NiveauEtudeSerializer, PosteSerializer,EvaluationSerializer,QuestionSerializer,ReponseSerializer,
CandidatSerializer,RecruteurSerializer,PasswordResetConfirmSerializer,PasswordResetSerializer,
UpdateCandidatSerializer,UpdateRecruteurSerializer)

from .models import Candidat, CentreInteret, Competence, ExperienceProfessionnelle, NiveauEtude, Poste,Evaluation,Question, Recruteur,Reponse
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import status
from .utils import authenticate_user,IsCandidat,IsRecruteur,saveFile,check_pk
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail,EmailMultiAlternatives
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view,action,permission_classes
from .models import CustomUser as User
import base64


class UserRegistration(APIView):
    def post(self,request):
        user_type = request.data.get('role')
        if user_type is None:
            return Response({'message':'user_type must be specified'},status=status.HTTP_400_BAD_REQUEST)
        if user_type == 'candidat':
            serializer = CandidatSerializer(data=request.data)
        elif user_type == 'recruteur':
            serializer = RecruteurSerializer(data=request.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if serializer.is_valid():
            user = serializer.save()
            refresh = RefreshToken.for_user(user)
            return Response({
                'refresh': str(refresh),
                'access': str(refresh.access_token)
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class CustomTokenObtainPairView(TokenObtainPairView):
#     def post(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         user = serializer.user
#         token = RefreshToken.for_user(user)
#         response = {
#             'refresh': str(token),
#             'access': str(token.access_token),
#             'role': user.role
#         }
#         return Response(response, status=status.HTTP_200_OK)
    
class LoginView(APIView):
    def post(self, request, *args, **kwargs):
        email = request.data.get("email")
        password = request.data.get("password")
        role = request.data.get("role")
        user = authenticate_user(email=email, password=password, role=role)
        if user is not None:
            refresh = RefreshToken.for_user(user)
            response = {
                'refresh': str(refresh),
                'access': str(refresh.access_token),
                'role': user.role
            }
            return Response(response, status=status.HTTP_200_OK)
        return Response({"detail": "Invalid email or password"}, status=status.HTTP_401_UNAUTHORIZED)


@csrf_exempt
@api_view(['POST'])
def password_reset(request):
    serializer = PasswordResetSerializer(data=request.data)
    if serializer.is_valid():
        email = serializer.validated_data['email']
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response({'error': 'Invalid email'}, status=status.HTTP_400_BAD_REQUEST)
        user_id = user.id
        crypt_user_id = base64.urlsafe_b64encode(str(user_id).encode()).decode()
        token = default_token_generator.make_token(user)
        subject = 'Password reset request'
        message = render_to_string('inscription/password_reset_email.html', {'email': user.email, 'token': token,'user_id':crypt_user_id})
        from_email = 'kapitalhc@example.com'
        to_email = email
        the_email = EmailMultiAlternatives(subject,'',to=[to_email],from_email=from_email)
        the_email.attach_alternative(message,"text/html")
        the_email.send()
        # send_mail(subject, message, from_email, [to_email])
        print(token,to_email)
        return Response({'success': 'Password reset email sent'}, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@csrf_exempt
@api_view(['POST'])
def password_reset_confirm(request,crypt_user_id,token):
    serializer = PasswordResetConfirmSerializer(data=request.data)
    if serializer.is_valid():
        password = serializer.validated_data['password']
        print(crypt_user_id,token,password)
        try:
            user_id = int(base64.urlsafe_b64decode(crypt_user_id))
        except:
            return Response({'error': 'User not found'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return Response({'error': 'Invalid email'}, status=status.HTTP_400_BAD_REQUEST)
        if not default_token_generator.check_token(user, token):
            return Response({'error': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)
        user.set_password(password)
        user.save()
        return Response({'success': 'Password reset successful'}, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateCandidatView(UpdateAPIView):
    permission_classes=[IsAuthenticated,IsCandidat]
    serializer_class=UpdateCandidatSerializer

    def get(self,request):
        candidat_profil = UpdateCandidatSerializer(self.get_object())
        return Response({'user':candidat_profil.data})
    def get_object(self):
        return Candidat.objects.get(pk=self.request.user.id)
    def put(self, request, *args, **kwargs):
        competences=json.loads(request.data['competences'])
        centre_interets=json.loads(request.data['centre_interets'])
        compt_list=[]
        user=self.get_object()
        for co in competences:
            obj = Competence.objects.get(id=co.get('id'))
            compt_list.append(obj)
        user.competences.set(compt_list)

        ci_list=[]
        for ce in centre_interets:
            obj=CentreInteret.objects.get(id=ce.get('id'))
            ci_list.append(obj)
        user.centre_interets.set(ci_list)

        return super().put(request, *args, **kwargs)    
              
    
    

class UpdateRecruteurView(UpdateAPIView):
    permission_classes=[IsAuthenticated,IsRecruteur]
    serializer_class=UpdateRecruteurSerializer
    def get(self,request):
        recruteur_profil = UpdateRecruteurSerializer(self.get_object())
        return Response({'user':recruteur_profil.data})

    def get_object(self):
        return Recruteur.objects.get(pk=self.request.user.id)

class CentreInteretViewSet(ReadOnlyModelViewSet):
    serializer_class=CentreInteretSerializer
    queryset=CentreInteret.objects.all()
    
class CompetenceViewSet(ReadOnlyModelViewSet):
    serializer_class=CompetenceSerializer
    queryset=Competence.objects.all()



class PosteViewSet(ReadOnlyModelViewSet):
    serializer_class = PosteSerializer
    queryset = Poste.objects.all()

class EvaluationViewSet(ModelViewSet):
    serializer_class = EvaluationSerializer
    queryset = Evaluation.objects.all()

class QuestionViewSet(ModelViewSet):
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()

class ReponseViewSet(ReadOnlyModelViewSet):
    serializer_class = ReponseSerializer
    queryset = Reponse.objects.all()

class NiveauEtudeViewSet(ReadOnlyModelViewSet):
    serializer_class = NiveauEtudeSerializer
    queryset = NiveauEtude.objects.all()

class ExperienceProfessionnelleViewSet(ModelViewSet):
    serializer_class=ExperienceProfessionnelleSerializer
    queryset=ExperienceProfessionnelle.objects.all()

    def get_permissions(self):
    
        if  self.action=='user_experiences' or self.request.method in ['PUT','PATCH','POST','DELETE']:
            permission_classes = [IsAuthenticated,IsCandidat]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]

    
    def update(self, request, *args, **kwargs):
        user_connected = Candidat.objects.get(pk=request.user.id)
        experience = get_object_or_404(ExperienceProfessionnelle,pk=kwargs['pk'])
        if experience.candidat != user_connected :
            return Response({'message':'action not allowed'},status=status.HTTP_401_UNAUTHORIZED)
        return super().update(request,*args,**kwargs)
    

    
    def destroy(self, request, *args, **kwargs):
        user_connected = Candidat.objects.get(pk=request.user.id)
        experience = get_object_or_404(ExperienceProfessionnelle,pk=kwargs['pk'])
        if experience.candidat != user_connected :
            return Response({'message':'action not allowed'},status=status.HTTP_401_UNAUTHORIZED)
        return super().destroy(request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        serializer = ExperienceProfessionnelleSerializer(data=request.data,context={'request':request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    


    #recupere tout les experiences profs associé à l'id de l'user passé en parametre
    @action(detail=True)
    def user_experiences(self, request,pk=None):
        try:
            safe_pk=check_pk(pk)
        except:
            return Response({'message':'argument not valid'},status=status.HTTP_400_BAD_REQUEST)
        try:
            user = Candidat.objects.get(pk=safe_pk)
        except:
            return Response({'message':'user not found'},status=status.HTTP_400_BAD_REQUEST)
        user_exps=ExperienceProfessionnelle.objects.filter(candidat=user)
        exp_serializer=ExperienceProfessionnelleSerializer(user_exps,many=True)
        return Response(exp_serializer.data)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    def partial_update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)