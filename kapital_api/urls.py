from rest_framework import routers
from .views import  CentreInteretViewSet, CompetenceViewSet, ExperienceProfessionnelleViewSet, NiveauEtudeViewSet, PosteViewSet,EvaluationViewSet,QuestionViewSet,ReponseViewSet

#enregistrement des routes 
router = routers.DefaultRouter()
router.register('poste',PosteViewSet)
router.register('evaluation',EvaluationViewSet)
router.register('question',QuestionViewSet)
router.register('reponse',ReponseViewSet)
router.register('centre_interet',CentreInteretViewSet)
router.register('competence',CompetenceViewSet)
router.register('niveau_etude',NiveauEtudeViewSet)
router.register('experience',ExperienceProfessionnelleViewSet)


