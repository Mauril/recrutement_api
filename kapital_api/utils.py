from django.conf import settings
from .models import CustomUser as User
from rest_framework.permissions import BasePermission
from django.core.files.storage import default_storage
from rest_framework.response import Response
from rest_framework import status

import os
def authenticate_user(email, password, role):
    try:
        if role == 'candidat':
            user = User.objects.get(email=email, role='candidat')
        elif role == 'recruteur':
            user = User.objects.get(email=email, role='recruteur')
        else:
            return None
        
        if user.check_password(password):
            return user
    except User.DoesNotExist:
        pass
    return None

def saveFile(request,name):
    the_path=os.path.join(settings.MEDIA_ROOT,'images')
    file =request.FILES['logo']
    print(file)
    saved_path=default_storage.save(file.name,content=file)
    print(default_storage.url(saved_path))
    return default_storage.url(saved_path)
def check_pk(pk):
    try :
        pk=int(pk)
        pk=abs(pk)
    except:
        raise Exception
    return pk

class IsCandidat(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.role == 'candidat')
    
class IsRecruteur(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.role == 'recruteur')
