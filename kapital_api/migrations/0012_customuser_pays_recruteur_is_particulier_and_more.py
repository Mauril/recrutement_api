# Generated by Django 4.2.1 on 2023-05-22 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapital_api', '0011_alter_candidat_cv_alter_candidat_lettre_motivation'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='pays',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='recruteur',
            name='is_particulier',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='recruteur',
            name='numero_cnss',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='recruteur',
            name='numero_ifu',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='recruteur',
            name='registre_commerce',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
