# Generated by Django 4.2.1 on 2023-05-11 15:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapital_api', '0006_alter_candidat_managers_alter_customuser_managers_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='candidat',
            options={},
        ),
        migrations.AlterModelOptions(
            name='customuser',
            options={},
        ),
        migrations.AlterModelOptions(
            name='recruteur',
            options={},
        ),
        migrations.AlterModelManagers(
            name='candidat',
            managers=[
            ],
        ),
        migrations.AlterModelManagers(
            name='customuser',
            managers=[
            ],
        ),
        migrations.AlterModelManagers(
            name='recruteur',
            managers=[
            ],
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='date_joined',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='is_staff',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='username',
        ),
        migrations.AlterField(
            model_name='customuser',
            name='email',
            field=models.EmailField(max_length=255, unique=True),
        ),
    ]
