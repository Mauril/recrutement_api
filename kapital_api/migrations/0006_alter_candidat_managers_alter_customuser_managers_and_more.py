# Generated by Django 4.2.1 on 2023-05-11 13:07

import django.contrib.auth.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kapital_api', '0005_alter_candidat_managers_alter_customuser_managers_and_more'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='candidat',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='customuser',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='recruteur',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
