# Generated by Django 4.2.1 on 2023-05-22 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kapital_api', '0010_customuser_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='candidat',
            name='cv',
            field=models.FileField(blank=True, null=True, upload_to='c_files/'),
        ),
        migrations.AlterField(
            model_name='candidat',
            name='lettre_motivation',
            field=models.FileField(blank=True, null=True, upload_to='c_files/'),
        ),
    ]
