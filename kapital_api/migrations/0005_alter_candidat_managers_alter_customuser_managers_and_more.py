# Generated by Django 4.2.1 on 2023-05-11 12:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kapital_api', '0004_alter_candidat_centre_interets_and_more'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='candidat',
            managers=[
            ],
        ),
        migrations.AlterModelManagers(
            name='customuser',
            managers=[
            ],
        ),
        migrations.AlterModelManagers(
            name='recruteur',
            managers=[
            ],
        ),
    ]
